//
//  MainViewController.h
//  TrainTicketQuery
//
//  Created by shiniv on 13-8-27.
//  Copyright (c) 2013年 visionsoar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainViewController : NSObject {
@private
    IBOutlet NSButton *search;
    IBOutlet NSTextField *start_time;
}

@end
