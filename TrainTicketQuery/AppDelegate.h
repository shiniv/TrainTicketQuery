//
//  AppDelegate.h
//  TrainTicketQuery
//
//  Created by shiniv on 13-8-19.
//  Copyright (c) 2013年 visionsoar. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
