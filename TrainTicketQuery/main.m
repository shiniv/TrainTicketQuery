//
//  main.m
//  TrainTicketQuery
//
//  Created by shiniv on 13-8-19.
//  Copyright (c) 2013年 visionsoar. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
